﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Versioning;
using System.IO;
using System.Diagnostics;
using System.Configuration.Install;
using System.Runtime.InteropServices;

namespace fdQueryDepend
{
    class Program
    {
        static void QueryDir(string sFolder)
        {
            DirectoryInfo TheFolder = new DirectoryInfo(sFolder);
            foreach (DirectoryInfo NextFolder in TheFolder.GetDirectories())
            {
                QueryDir(NextFolder.FullName);
            }
            foreach (FileInfo NextFile in TheFolder.GetFiles())
            {
                if (string.Compare(NextFile.Extension,".exe", true)==0 ||
                    string.Compare(NextFile.Extension, ".dll", true) == 0)
                {
                    try
                    {
                        var a = System.Reflection.Assembly.LoadFile(NextFile.FullName);
                        Console.WriteLine(a.ImageRuntimeVersion + "  :  " + a.ToString()+" : "+ NextFile.FullName);
                    }
                    catch (System.Exception ex)
                    {
                        Trace.WriteLine(NextFile.FullName+" File is not C#.");
                    }
                }

            }

        }
  //      [DllImport("SendMail.dll", CallingConvention = CallingConvention.Cdecl)]
  //      public static extern bool SendMail(string address, string subject, string head, string attach);

        static int Main(string[] args)
        {
            int ret = 0;
            //DISM /Online /Enable-Feature /FeatureName:NetFx3 /All /LimitAccess /Source:d:\sources\sxs
            string sxspath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "sxs");
            if (System.IO.Directory.Exists(sxspath))
            {
                StringBuilder sb = new StringBuilder();
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = "cmd.exe";
                p.StartInfo.Arguments = string.Format("/C DISM /Online /Enable-Feature /FeatureName:NetFx3 /All /LimitAccess /Source:\"{0}\"", sxspath);
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.OutputDataReceived += new System.Diagnostics.DataReceivedEventHandler(
                    delegate(object sender, System.Diagnostics.DataReceivedEventArgs e)
                    {
                        if (!string.IsNullOrEmpty(e.Data))
                        {
                            sb.Append(e.Data);
                        }
                    });
                p.Start();
                p.BeginOutputReadLine();
                p.WaitForExit(15 * 60 * 1000);
                ret = p.ExitCode;
                if (ret == 0)
                {
                }
                else
                {
                    Trace.WriteLine(string.Format("unzip return fail, err: {0}", ret));
                    ret = 10;
                }
            }
            else
            {
                Trace.WriteLine(string.Format("unzip is not exist. path: {0}", sxspath));
                ret = 20;
            }

            return ret;

            //if (SendMail("jefferyz@futuredial.com", "adbcad", "title\r\n", @"J:\temp\logfile.zip"))
            //{
            //    Console.WriteLine("send Mail");
            //}



            //var a = System.Reflection.Assembly.LoadFile(@"C:\Users\jeffery\Desktop\HttpAnalyzer\keygen.exe");
            //Console.WriteLine(a.ToString() +"  :  "+ a.ImageRuntimeVersion);
            //InstallContext _args = new InstallContext(null, args);
            //if (_args.Parameters.ContainsKey("dir") && !string.IsNullOrEmpty(_args.Parameters["dir"]))
            {
               // QueryDir(_args.Parameters["dir"]);
            }
        }
    }
}
